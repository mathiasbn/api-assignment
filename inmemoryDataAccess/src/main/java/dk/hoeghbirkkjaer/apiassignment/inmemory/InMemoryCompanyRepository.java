package dk.hoeghbirkkjaer.apiassignment.inmemory;

import dk.hoeghbirkkjaer.apiassignment.core.Company;
import dk.hoeghbirkkjaer.apiassignment.core.CompanyRepository;
import dk.hoeghbirkkjaer.apiassignment.core.CompanySpec;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
@Scope("singleton")
public class InMemoryCompanyRepository implements CompanyRepository {
    private final List<Company> companies = new ArrayList<>();

    @Override
    public List<Company> all() {
        return new ArrayList<>(companies);
    }

    /**
     * Emulating a database, by creating a new instance in the store, and not changing the reference
     */
    @Override
    public void replace(int id, CompanySpec specification) {
        getById(id).orElseThrow(() -> new RuntimeException("Company not found"));
        companies.removeIf(c->c.getId() == id);
        companies.add(new Company(id, specification));
    }

    @Override
    public Optional<Company> getById(int id) {
        return companies.stream().filter(c->c.getId() == id).findFirst();
    }

    @Override
    public Company add(CompanySpec specification) {
        int id = companies.size() + 1;
        Company company = new Company(id, specification);
        companies.add(company);
        return company;
    }
}
