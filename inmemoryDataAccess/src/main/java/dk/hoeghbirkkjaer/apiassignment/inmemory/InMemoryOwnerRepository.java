package dk.hoeghbirkkjaer.apiassignment.inmemory;

import dk.hoeghbirkkjaer.apiassignment.core.*;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
@Scope("singleton")
public class InMemoryOwnerRepository implements OwnerRepository {
    private final List<Owner> companies = new ArrayList<>();

    @Override
    public Optional<Owner> getById(int id) {
        return companies.stream().filter(c->c.getId() == id).findFirst();
    }

    @Override
    public Owner add(OwnerSpec specification) {
        int id = companies.size() + 1;
        Owner owner = new Owner(id, specification);
        companies.add(owner);
        return owner;
    }
}
