plugins {
    java
}
subprojects {
    apply {
        plugin<JavaPlugin>()
    }
    repositories {
        mavenCentral()
        maven {
            url = uri("file://${rootDir}/mvnRepo")
        }
    }

    dependencies {
        testImplementation ("org.junit.jupiter:junit-jupiter:5.6.0")
    }

    tasks.withType<Test> {
        useJUnitPlatform()
    }
}