package dk.hoeghbirkkjaer.apiassignment.core;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * I do realise 1111 is not valid (I think), and that I do not test for date
 */
class SocialSecurityNumberTest {

    @Test
    public void validSocialSecurityNumbers() {
        assertTrue(SocialSecurityNumber.parse("140983-1111").isPresent());
        assertTrue(SocialSecurityNumber.parse("1409831111").isPresent());
    }

    @Test
    public void invalidSocialSecurityNumbers() {
        assertFalse(SocialSecurityNumber.parse("140983").isPresent());
        assertFalse(SocialSecurityNumber.parse("140983--1111").isPresent());
    }
}