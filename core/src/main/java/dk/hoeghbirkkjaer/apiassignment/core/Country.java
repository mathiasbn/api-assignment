package dk.hoeghbirkkjaer.apiassignment.core;

import java.util.Arrays;
import java.util.Optional;

public enum Country {
    DENMARK("Denmark"), NORWAY("Norway"), SWEDEN("Sweden");

    public String presentation;

    Country(String presentation) {
        this.presentation = presentation;
    }

    public static Optional<Country> parse(String country) {
        return Arrays.stream(values()).filter(c->c.presentation.equals(country)).findFirst();
    }
}
