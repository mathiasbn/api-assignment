package dk.hoeghbirkkjaer.apiassignment.core;

import java.util.Optional;

public class SocialSecurityNumber {

    private String value;

    public static Optional<SocialSecurityNumber> parse(String text){
        if(text.matches("\\d{6}-\\d{4}"))
            text = text.replace("-","");
        if(!text.matches("\\d{10}"))
            return Optional.empty();
        return Optional.of(new SocialSecurityNumber(text));
    }

    public SocialSecurityNumber(String value) {
        this.value = value;
    }

    public String presentation(){
        return value;
    }
}
