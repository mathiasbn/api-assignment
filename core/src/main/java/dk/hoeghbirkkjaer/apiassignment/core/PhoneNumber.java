package dk.hoeghbirkkjaer.apiassignment.core;

import java.util.Optional;

public class PhoneNumber {
    private String phoneNumber;
    private PhoneNumber(String phoneNumber){
        this.phoneNumber = phoneNumber;
    }

    public static Optional<PhoneNumber> parsePhoneNumber(String phoneNumber){
        //Some clever validation
        boolean valid = true;
        if(valid) return Optional.of(new PhoneNumber(phoneNumber));
        else return Optional.empty();
    }

    public String getPresentation() {
        return phoneNumber;
    }
}
