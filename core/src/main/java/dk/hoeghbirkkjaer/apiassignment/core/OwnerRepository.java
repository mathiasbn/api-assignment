package dk.hoeghbirkkjaer.apiassignment.core;

import java.util.Optional;

public interface OwnerRepository {
    Optional<Owner> getById(int id);

    Owner add(OwnerSpec owner);
}
