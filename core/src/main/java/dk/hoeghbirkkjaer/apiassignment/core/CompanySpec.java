package dk.hoeghbirkkjaer.apiassignment.core;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CompanySpec {
    private String name;
    private Country country;
    private PhoneNumber phoneNumber;
    private final Set<Integer> owners = new HashSet<>();

    public CompanySpec(String name, Country country, PhoneNumber phoneNumber) {
        this.name = name;
        this.country = country;
        this.phoneNumber = phoneNumber;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public void setPhoneNumber(PhoneNumber phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void addCompanyOwner(int owner) {
        owners.add(owner);
    }

    public List<Integer> getOwners() {
        return new ArrayList<>(owners);
    }

    public String getName() {
        return name;
    }

    public Country getCountry() {
        return country;
    }

    public PhoneNumber getPhoneNumber() {
        return phoneNumber;
    }

}
