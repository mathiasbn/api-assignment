package dk.hoeghbirkkjaer.apiassignment.core;

import java.util.List;
import java.util.Optional;

public interface CompanyRepository {
    Optional<Company> getById(int id);

    Company add(CompanySpec company);

    List<Company> all();

    void replace(int id, CompanySpec toSpecification);
}
