package dk.hoeghbirkkjaer.apiassignment.core;


public class Owner extends OwnerSpec{
    private int id;

    public Owner(int id, String name, SocialSecurityNumber socialSecurityNumber) {
        super(name,socialSecurityNumber);
        this.id = id;
    }

    public Owner(int id, OwnerSpec specification) {
        this(id, specification.getName(), specification.getSocialSecurityNumber());
    }

    public int getId() {
        return id;
    }
}
