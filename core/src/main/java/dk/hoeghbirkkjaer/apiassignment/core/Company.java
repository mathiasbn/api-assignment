package dk.hoeghbirkkjaer.apiassignment.core;

public class Company extends CompanySpec {

    private int id;

    public Company(int id, String name, Country country, PhoneNumber phoneNumber) {
        super(name, country, phoneNumber);
        this.id = id;
    }

    public Company(int id, CompanySpec specification) {
        this(id, specification.getName(),specification.getCountry(),specification.getPhoneNumber());
    }

    public int getId() {
        return id;
    }
}
