package dk.hoeghbirkkjaer.apiassignment.core;

public class OwnerSpec {
    private String name;
    private SocialSecurityNumber socialSecurityNumber;

    public OwnerSpec(String name, SocialSecurityNumber socialSecurityNumber) {
        this.name = name;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SocialSecurityNumber getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public void setSocialSecurityNumber(SocialSecurityNumber socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }
}
