plugins {
    `java-library`
}

dependencies {
    api("dk.hoeghbirkkjaer.apiassignment:api:1.0.4")
    implementation("org.springframework:spring-web:5.3.3")
    implementation(project(":core"))
}
