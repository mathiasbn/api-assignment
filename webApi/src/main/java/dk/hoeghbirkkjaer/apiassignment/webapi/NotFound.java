package dk.hoeghbirkkjaer.apiassignment.webapi;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
class NotFound extends RuntimeException {

}
@ResponseStatus(HttpStatus.BAD_REQUEST)
class BadRequest extends RuntimeException {

}
