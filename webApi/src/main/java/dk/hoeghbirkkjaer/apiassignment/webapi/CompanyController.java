package dk.hoeghbirkkjaer.apiassignment.webapi;

import dk.hoeghbirkkjaer.apiassignment.api.Company;
import dk.hoeghbirkkjaer.apiassignment.core.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

@RestController
public class CompanyController {

    private final CompanyRepository repository;
    private OwnerRepository ownerRepository;

    @Autowired
    public CompanyController(CompanyRepository repository, OwnerRepository ownerRepository) {
        this.repository = repository;
        this.ownerRepository = ownerRepository;
    }

    @GetMapping(path = "/companies/{id}")
    public Company getById(@PathVariable int id) {
        return repository.getById(id)
                         .map(this::mapToApiType)
                         .orElseThrow(NotFound::new);
    }

    @PostMapping("/companies/")
    public int addCompany(@RequestBody Company company) {
        //The api could be more polite and return a message of why its not valid
        CompanySpec specification = toSpecification(company);
        return repository.add(specification).getId();
    }

    @PutMapping("/companies/{id}")
    public void update(@PathVariable int id, @RequestBody Company company) {
        repository.replace(id, toSpecification(company));
    }

    @GetMapping("/companies/")
    public List<Company> list() {
        return repository.all().stream().map(this::mapToApiType).collect(toList());
    }

    @PutMapping("/companies/{id}/owners/{ownerId}")
    public void setOwner(@PathVariable int id, @PathVariable int ownerId) {
        ownerRepository.getById(ownerId).orElseThrow(NotFound::new);
        repository.getById(id).orElseThrow(NotFound::new).addCompanyOwner(ownerId);
    }

    @GetMapping("/companies/{id}/owners/")
    public List<OwnerIdentifier> listOwner(@PathVariable int id) {
        return repository.getById(id).orElseThrow(NotFound::new)
                         .getOwners().stream().map(OwnerIdentifier::new).collect(Collectors.toList());
    }

    private CompanySpec toSpecification(Company company) {
        PhoneNumber phoneNumber = PhoneNumber.parsePhoneNumber(company.getPhoneNumber())
                                             .orElseThrow(BadRequest::new);
        Country country = Country.parse(company.getCountry())
                                 .orElseThrow(BadRequest::new);
        return new CompanySpec(company.getCompanyName(), country, phoneNumber);
    }

    private Company mapToApiType(dk.hoeghbirkkjaer.apiassignment.core.Company company) {
        return new Company(company.getId(), company.getName(), company.getCountry().presentation,
                           company.getPhoneNumber().getPresentation());
    }

    private static class OwnerIdentifier {
        int id;

        public OwnerIdentifier() {
        }

        public OwnerIdentifier(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }
}

