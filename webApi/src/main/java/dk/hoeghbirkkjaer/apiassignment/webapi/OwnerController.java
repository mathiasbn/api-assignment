package dk.hoeghbirkkjaer.apiassignment.webapi;

import dk.hoeghbirkkjaer.apiassignment.api.Owner;
import dk.hoeghbirkkjaer.apiassignment.core.OwnerRepository;
import dk.hoeghbirkkjaer.apiassignment.core.OwnerSpec;
import dk.hoeghbirkkjaer.apiassignment.core.SocialSecurityNumber;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class OwnerController {

    private final OwnerRepository repository;

    @Autowired
    public OwnerController(OwnerRepository repository) {
        this.repository = repository;
    }

    @GetMapping(path = "/owners/{id}")
    public Owner getById(@PathVariable int id) {
        return repository.getById(id)
                         .map(this::mapToApiType)
                         .orElseThrow(NotFound::new);
    }

    @PostMapping("/owners/")
    public int addOwner(@RequestBody Owner owner) {
        //The api could be more polite and return a message of why its not valid
        SocialSecurityNumber socialSecurityNumber = SocialSecurityNumber.parse(owner.getSocialSecurityNumber())
                                                                        .orElseThrow(BadRequest::new);
        return repository.add(new OwnerSpec(owner.getName(), socialSecurityNumber))
                         .getId();
    }

    private Owner mapToApiType(dk.hoeghbirkkjaer.apiassignment.core.Owner owner) {
        return new Owner(owner.getId(), owner.getName(), owner.getSocialSecurityNumber().presentation());
    }
}

