plugins {
    java
    id("org.springframework.boot") version "2.4.2"
    id ("io.spring.dependency-management") version "1.0.8.RELEASE"
    id("com.bmuschko.docker-spring-boot-application") version "6.7.0"
}

project.group = "dk.hoeghbirkkjaer"
project.version = "1.0.0"

dependencies {
    runtimeOnly(project(":webApi"))
    testImplementation(project(":webApi"))
    runtimeOnly(project(":core"))
    runtimeOnly(project(":inmemoryDataAccess"))
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("com.google.code.gson:gson:2.8.5")

    testImplementation(group = "org.springframework.boot", name = "spring-boot-starter-test", version = "2.4.2")
    {
        exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
    }

    docker {
        springBootApplication {
            baseImage.set("openjdk:15.0.2-jdk")
            images.set(setOf("${project.group}/ApiAssignment:${project.version}"))
        }
    }
}
