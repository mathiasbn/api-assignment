package dk.hoeghbirkkjaer.apiassignment;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import dk.hoeghbirkkjaer.apiassignment.api.Company;
import dk.hoeghbirkkjaer.apiassignment.api.Owner;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.function.BiFunction;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class CompanyControllerTest {

    @Autowired
    private MockMvc mvc;

    @Test
    public void getNotFound() throws Exception {
        mvc.perform(get("/companies/0"))
           .andExpect(status().isNotFound());
    }

    @Test
    public void addGetCompany() throws Exception {
        Company toAdd = new Company(0, "newCompany", "Sweden", "88888888");
        int id = postCompany(toAdd);
        assertCompanyWhenGet(id, toAdd);
    }

    @Test
    public void listCompanies() throws Exception {
        List<Company> companies = Arrays.asList(new Company(0, "company1", "Denmark", "123"),
                                                new Company(0, "company2", "Denmark", "123"),
                                                new Company(0, "company3", "Denmark", "123"));

        companies.forEach(c -> c.setId(postCompany(c)));

        mvc.perform(get("/companies/")).andExpect(result -> {
            List<Company> retrievedCompanies = objectResult(result, new TypeToken<List<Company>>() {}.getType());
            companies.forEach(c -> assertEquals(c, retrievedCompanies.stream()
                                                                     .filter(r -> r.getId() == c.getId())
                                                                     .findFirst()
                                                                     .get()));
        });
    }

    @Test
    public void updateCompany() throws Exception {
        int id = postCompany(new Company(0, "newCompany", "Sweden", "88888888"));
        Company updated = new Company(0, "updatedCompany", "Denmark", "12345678");
        perform(this::putJson,"/companies/" + id, updated);
        assertCompanyWhenGet(id, updated);
    }

    @Test
    public void putRetrieveOwners() throws Exception {
        Company toAdd = new Company(0, "newCompany", "Sweden", "88888888");
        int id = postCompany(toAdd);
        int ownerId = postOwner(new Owner(0, "TheBoss", "123456-7890"));
        perform(this::putJson,"/companies/" + id + "/owners/"+ownerId,null);

        List<LinkedHashMap<String, Integer>> ownerIds = objectResult(
                perform(MockMvcRequestBuilders::get, "/companies/" + id + "/owners/", null),
                new TypeToken<List<LinkedHashMap<String, Integer>>>() {}.getType());
        assertIterableEquals(List.of(ownerId), ownerIds.stream().map(o->o.get("id")).collect(toList()));
    }

    private void assertCompanyWhenGet(int id, Company toAdd) throws Exception {
        mvc.perform(get("/companies/" + id))
           .andExpect(status().isOk())
           .andExpect(jsonPath("companyName", equalTo(toAdd.getCompanyName())))
           .andExpect(jsonPath("country", equalTo(toAdd.getCountry())))
           .andExpect(jsonPath("phoneNumber", equalTo(toAdd.getPhoneNumber())))
        ;
    }

    private int postCompany(Company toAdd) {
        return intResult(perform(this::postJson, "/companies/", toAdd));
    }

    private int postOwner(Owner toAdd) {
        return intResult(perform(this::postJson, "/owners/", toAdd));
    }

    private MvcResult perform(BiFunction<String, Object, MockHttpServletRequestBuilder> method,
                              String urlTemplate,
                              Object toAdd) {
        try {
            return mvc.perform(method.apply(urlTemplate, toAdd))
                      .andExpect(status().isOk())
                      .andReturn();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private MockHttpServletRequestBuilder postJson(String urlTemplate, Object obj) {
        return post(urlTemplate)
                .content(asJsonString(obj))
                .contentType(MediaType.APPLICATION_JSON);
    }

    private MockHttpServletRequestBuilder putJson(String urlTemplate, Object obj) {
        return put(urlTemplate)
                .content(asJsonString(obj))
                .contentType(MediaType.APPLICATION_JSON);
    }

    private int intResult(org.springframework.test.web.servlet.MvcResult result) {
        try {
            return Integer.parseInt(result.getResponse().getContentAsString());
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    private <T> T objectResult(org.springframework.test.web.servlet.MvcResult result, Type type) {
        try {
            return new Gson().fromJson(result.getResponse().getContentAsString(),
                                       type);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public static String asJsonString(final Object obj) {
        return new Gson().toJson(obj);
    }
}
