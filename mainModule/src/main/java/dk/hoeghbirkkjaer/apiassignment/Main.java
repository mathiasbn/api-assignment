package dk.hoeghbirkkjaer.apiassignment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "dk.hoeghbirkkjaer.apiassignment")
public class Main {
    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }
}
