plugins {
    java
    `maven-publish`
}



publishing {
    repositories {
        maven {
            name = "ProjectRepo"
            url = uri("file://${rootDir}/mvnRepo")
        }
    }

    publications {
        create<MavenPublication>("api") {
            groupId = "dk.hoeghbirkkjaer.apiassignment"
            artifactId = "api"
            version = "1.0.4"

            from(components["java"])
        }
    }
}