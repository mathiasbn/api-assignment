package dk.hoeghbirkkjaer.apiassignment.api;

import java.util.Objects;

public class Company{
    private int id;
    private String companyName;
    private String country;
    private String phoneNumber;

    private Company() {
    }

    public Company(int id, String companyName, String country, String phoneNumber) {
        this.id = id;
        this.companyName = companyName;
        this.country = country;
        this.phoneNumber = phoneNumber;
    }

    public int getId() {
        return id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getCountry() {
        return country;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Company company = (Company) o;
        return id == company.id && Objects.equals(companyName, company.companyName) &&
               Objects.equals(country, company.country) &&
               Objects.equals(phoneNumber, company.phoneNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, companyName, country, phoneNumber);
    }
}
