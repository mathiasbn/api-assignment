package dk.hoeghbirkkjaer.apiassignment.api;

import java.util.Objects;

public class Owner {
    private int id;
    private String name;
    private String socialSecurityNumber;

    private Owner() {
    }

    public Owner(int id, String name, String socialSecurityNumber) {
        this.id = id;
        this.name = name;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Owner owner = (Owner) o;
        return id == owner.id && Objects.equals(name, owner.name) &&
               Objects.equals(socialSecurityNumber, owner.socialSecurityNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, socialSecurityNumber);
    }
}
