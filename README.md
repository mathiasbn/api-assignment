# API assignment

This project is an answer to the given code assignment.

It's a Java Spring Boot application. 

## Web API
It holds a webapi with to sections:

- /companies/
- /owners/

### Companies API
**Lookup by id:**

`curl http://localhost:8080/companies/{id}`

**List all:**

`curl http://localhost:8080/companies/`

**Create a new company:**

`curl -XPOST -H "Content-type: application/json" -d '{"companyName":"myCompany","country":"Denmark","phoneNumber":"12345678"}' 'http://localhost:8080/companies/'`

**Update:**

`curl -XPUT -H "Content-type: application/json" -d '{"companyName":"myChangedCompany","country":"Denmark","phoneNumber":"12345678"}' 'http://localhost:8080/companies/{id}'`

**Get owners of a company:**

`curl 'http://localhost:8080/companies/{id}/owners/`

**Set an owner:**

`curl -XPUT -H "Content-type: application/json" 'http://localhost:8080/companies/{id}/owners/{ownerId}'`

### Owners API
**Lookup by id:**

`curl http://localhost:8080/owners/{id}`

**Create a new owner:**

`curl -XPOST -H "Content-type: application/json" -d '{"name":"myBoss","socialSecurityNumber":"123456-7890"}' 'http://localhost:8080/owners/'`

## Build system
The build system is gradle. The gradle distribution will automatically be downloaded.
The only prerequisite is a JDK.

**To build the application**

`./gradlew build`

**To create a dockerfile**

`./gradlew dockerCreateDockerfile`

**To build a docker image**

`./gradlew dockerBuildImage`

> WARNING: build image did't work so well for me. If it hangs, see below

## Build docker container
**To build an image. (from project root)**

`docker build -t dk.hoeghbirkkjaer/api-assignment:1.0.0 mainModule/build/docker/`

**To run container**

`docker run -d -p 8080:8080 --name "api-assignment" dk.hoeghbirkkjaer/api-assignment:1.0.0`

## Authorization and Authentication
In a new future this project should probably implement some auth feature.
One common way to do this, would be to have another trusted service to handle auth with eg OAuth2, and then let the user receive a JWT token.

**Authentication**

This token will the be send along with every request to the API (The authentication part). 

**Authorization**

A JWT token can also contain various claims, that this project would expect. These claims would give access to different parts of the exposed data.
Fx. could some users be disalowed to receive social security numbers. 

**Benefits**

Benefits of this approch is among other things:

- that you delegate the auth responsablility to another service. This API will therefore not need to manage users and passwords/secrets
- Also a JWT can be verified locally (With the authservers public certificate installed)
- And last, a token is a temporary access token that can be used multiple times in a short period, but avoid sending your credentials multiple times (in one session).
